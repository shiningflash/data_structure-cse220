package lab01;

/* Given an array of ints,
 * return true if 6 appears as either the first or. Last element in the array.
 * The array will be length 1 or more. 
 * firstLast6 ({1, 2, 6}) → true
 * firstLast6 ({6, 1, 2, 3}) → true
 * firstLast6 ({13, 6, 1, 2, 3}) → false 
*/

public class Task01 {
	public static boolean check6(int array[]) {
		boolean flag = false;
		if (array[0] == 6 || array[array.length-1] == 6)
			flag = true;
		return flag;
	}
	public static void main (String[] args) {
		int array1[] = {1, 2, 6};
		int array2[] = {6, 1, 2, 3};
		int array3[] = {13, 6, 1, 2, 3};
		
		System.out.println(check6(array1));
		System.out.println(check6(array2));
		System.out.println(check6(array3));
	}
}
