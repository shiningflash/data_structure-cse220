package lab01;

/* Write a program which reads 5 numbers into an array
 * sorts/arranges the numbers from low to high
 * and prints all numbers in the array
 * and the even number in that list.
 * If the user enters 7, 13, 2, 10, 6
 * then your program should print 13, 10, 7, 6, 2, 10, 6 , 2.
 * 
 * input check: 7 13 2 10 6
*/

import java.util.Scanner;
public class Task03 {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = new int[5];
		int even[] = new int[5];
		int i, j, k = 0;
		for (i = 0; i < 5; i++) {
			array[i] = sc.nextInt();
		}
		for (i = 0; i < array.length; i++) {
			for (j = 1; j < array.length; j++) {
				if (array[j-1] < array[j]) {
					int temp = array[j-1];
					array[j-1] = array[j];
					array[j] = temp;
				}
			}
		}
		for(i = 0; i < array.length; i++) {
			if (array[i] % 2 == 0)
				even[k++] = array[i];
		}
		for (i = 0; i < 5; i++) {
			System.out.print(array[i] + ", ");
		}
		for (i = 0; i < k-1; i++) {
			System.out.print(even[i] + ", ");
		}
		System.out.println(even[k-1]);
		sc.close();
	}
}