package lab01;

import java.util.Arrays;
//import org.apache.commons.lang.ArrayUtils;

public class Task13{
	  public static int [] removeOdd (int[] input){
		  int len = input.length;
		  int num = 0;
		  for (int i = 0; i < len; i++) {
			  if (input[i] % 2 != 0) {
//				  System.out.print(input[i] + " "); // odd number
				  input[i] = -1;
				  num++;
			  }
		  }
		  int a = 0;
		  int noOdd[] = new int[len-num];
		  for (int i = 0; i < len; i++) {
			  if (input[i] != -1) 
				  noOdd[a++] = input[i];
		  }
		  return noOdd;
	  }
	  public static void main(String [] args){
	    int[] mixedArray = {21, 33, 44, 66, 11, 1, 88, 45, 10, 9};
	    for (int i = 0; i < mixedArray.length; i++) {
	      System.out.print(mixedArray[i] + " ");
	    }
	    System.out.println();
	    int[] noOdd = removeOdd(mixedArray);
	    System.out.print("\nnoOdd: ");
	    for (int i = 0; i < noOdd.length; i++) {
	      System.out.print(noOdd[i] + " ");
	    }    
	}
}