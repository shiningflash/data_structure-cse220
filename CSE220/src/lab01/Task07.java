package lab01;

/* Read 10 input from the user, and store them in an array
 * Then ask the user to give another input between 0 and 9 for choosing an array cell.
 * Display a line with that many "*" characters.
 * 
 * Eg. Say if the user enter 15 6 7 9 45 2 26 84 8 10
 * then the user again give input as 3 the output will be “ *********” (9 stars)
 */

import java.util.Scanner;
public class Task07 {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = new int[10];
		System.out.println("Enter any 10 numbers for array:");
		for (int i = 0; i < 10; i++) {
			array[i] = sc.nextInt();
		}
		System.out.println("Enter a number: ");
		int n = sc.nextInt();
		System.out.print("\nOutput: ");
		for (int i = 0; i < array[n]; i++) {
			System.out.print('*');
		}
		System.out.println();
		sc.close();
	}
}
