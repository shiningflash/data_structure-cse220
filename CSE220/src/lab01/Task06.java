package lab01;

/* An array is a palindrome if it reads the same in both directions
 * 
 * For example, 3 5 7 2 4 is not a palindrome;
 * however, the following one is. 4 2 6 2 
 * Write a program that reads in an array and checks if it is a palindrome
*/

import java.util.Scanner;
public class Task06 {
	public static boolean is_palindrome(int array[]) {
		boolean flag = true;
		int len = array.length;
		int k = len;
		for (int i = 0; i < len/2; i++) {
			if (array[i] != array[--k]) {
				flag = false;
				break;
			}
		}
		return flag;
	}
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = new int[6];
		System.out.println("Enter 6 number for an array:");
		for (int i = 0; i < array.length; i++) {
			array[i] = sc.nextInt();
		}
		System.out.println("\nis palindrome? : " + is_palindrome(array));
		sc.close();
	}
}
