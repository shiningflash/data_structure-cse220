package lab01;

/* Write a Java Code of a program that declares an array of integer type of size 10 and takes input of your choice.
 * Have the program print the values, square all the values, and then display the new values.
 * Write a static method (in the same class as main method) to do the displaying and a second static method to find out the squares.
 * Have the methods take the array name and the array size as arguments. 
 */

import java.util.Scanner;
import java.text.DecimalFormat;
public class Taask09 {
	static DecimalFormat f = new DecimalFormat("#.00");
	public static void display(double sqrtARRAY[]) {
		for (int i = 0; i < 10; i++) {
			System.out.println(f.format(sqrtARRAY[i]));
		}
	}
	public static double[] sqrtARRAY(int array[]) {
		double sqrtARRAY[] = new double [array.length];
		for (int i = 0; i < 10; i++) {
			sqrtARRAY[i] = Math.sqrt(array[i]);
		}
		return sqrtARRAY;
	}
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = new int[10];
		for (int i = 0; i < 10; i++) {
			array[i] = sc.nextInt();
		}
		double sqrtARRAY[] = sqrtARRAY(array);
		display(sqrtARRAY);
	}
}
