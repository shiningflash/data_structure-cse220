package lab01;

/* Write a Java program that would ask the user to enter ten numbers
 * As the user enters the ten numbers the program will make sure that all the numbers entered by the user are unique.
 * For example if the user enters 2, 3, 4, 6,
 * and then tries to enter 3 again the program will display that 3 is already in among the entered numbers
 * and would ask the user to enter a new number.
 */

import java.util.Scanner;
public class Task04 {	
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = new int[10];
		array[0] = sc.nextInt();
		for (int i = 1; i < 10; i++) {
			array[i] = sc.nextInt();
			for (int j = 0; j < i; j++) {
				if (array[i] == array[j]) {
					System.out.println(array[i] + " alresdy exist. Please enter another unique one.");
					i--; break;
				}
			}
		}
		for (int i = 0; i < 9; i++)
			System.out.print(array[i] + " ");
		System.out.println(array[9]);
	}
}
