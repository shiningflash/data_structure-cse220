package lab01;

/* Ask user for dimension/size of two row matrices
 * take all values in each matrix and print the summation.
 * If matrices are A and B, then you need to calculate C=5A-B.
 * For example, if the user enters 3, then size of each matrix is 3.
 * Then you need to take 3 values for first matrix which will form A = [5 6 7].
 * Then take 3 values for second matrix, B = [2 3 4].
 * You need to print result, C = [23 27 31].
 * The output will be 23 27 31.
 * You must use array, loop (while or for), and array.length for this program.
 */

import java.util.Scanner;
public class Task08 {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter length: ");
		int length = sc.nextInt();
		int matrixA[] = new int[length];
		int matrixB[] = new int[length];
		System.out.print("Matrix A: ");
		for (int i = 0; i < length; i++) {
			matrixA[i] = sc.nextInt();
		}
		System.out.print("Matrix B: ");
		for (int i = 0; i < length; i++) {
			matrixB[i] = sc.nextInt();
		}
		System.out.print("\nMatrix C: ");
		for (int i = 0; i < length; i++) {
			System.out.print(5 * matrixA[i] - matrixB[i] + " ");
		}
		System.out.println();
		sc.close();
	}
}