package lab01;

/* Write a Java program that would ask the user to enter 15 numbers
 * All the numbers are between 0 and 9.
 * The program should count how many time the number been given. 
 * 
 * In the Input:
 * 1 1 2 5 6 8 7 2 4 6 9 4 2 5 6
 * 
 * Output should look like: 
 * 0 was found 0 times
 * 1 was found 2 times
 * 2 was found 3 times
 * 3 was found 0 times
 * 4 was found 2 times
 * 5 was found 2 times
 * 6 was found 3 times
 * 7 was found 1 times
 * 8 was found 1 times
 * 9 was found 1 times
 * 
 * Hint: Using an array,
 * use the element a[n] to count frequency / occurrence of the number n.
*/

import java.util.Scanner;
public class Task05 {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int a[] = new int[10];
		int i = 0, num = 0;
		for (i = 0; i < 15; i++) {
			num = sc.nextInt();
			a[num]++;
		}
		for (i = 0; i <= 9; i++) {
			System.out.println(i + " was found " + a[i] + " times");
		}
	}
}
