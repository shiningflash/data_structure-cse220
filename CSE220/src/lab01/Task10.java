package lab01;

import java.util.Scanner;
public class Task10 {
  
  public static String Season(int month, int day) {
    String season = null;
    
    if (month > 12 || day > 31)
      season = "Wrong Input";
    
    else if ((month == 12 && day >= 16) || (month >= 1 && month <= 2) || (month == 3 && day <= 15))
      season = "Winter";
      
    else if ((month == 3 && day >= 16) || (month >= 4 && month <= 5) || (month == 6 && day <= 15))
      season = "Spring";
    
    else if ((month == 6 && day >= 16) || (month >= 7 && month <= 8) || (month == 9 && day <= 15))
      season = "Summer";
    
    else if ((month == 9 && day >= 16) || (month >= 10 && month <= 11) || (month == 12 && day <= 15))
      season = "Fall";
    
    return season;
  }
  
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    while(sc.hasNext()) {
      int month = sc.nextInt();
      int day = sc.nextInt();
      System.out.println(Season(month, day));
    }
  }
}