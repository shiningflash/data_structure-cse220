package lab01;

/* Read in 10 input from the keyboard, and store them in an array,
 * Find the position (or index) of the maximum and minimum values in the array,
 * and swap them (move the biggest element to the position of the smallest,
 * and move the smallest element to the position of the biggest) and print that again.
 * If the user enters 7, 13, -5, 10, 6 then your program should print 7, -5, 13, 10, 6
 */

import java.util.Scanner;
public class Task02 {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = new int[10];
		array[0] = sc.nextInt();
		int min = array[0], max = array[0], minLocation = 0, maxLocation = 0;
		for (int i = 1; i < 10; i++) {
			array[i] = sc.nextInt();
			if (min > array[i]) {
				min = array[i]; minLocation = i;
			}
			if (max < array[i]) {
				max = array[i]; maxLocation = i;
			}
		}
		int temp = array[minLocation];
		array[minLocation] = array[maxLocation];
		array[maxLocation] = temp;
		for (int i = 0; i < 9; i++) {
			System.out.print(array[i] + ", ");
		}
		System.out.println(array[9]);
		sc.close();
	}
}
