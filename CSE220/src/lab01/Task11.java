package lab01;

/*
 * Write a method named allDigitsOdd that returns whether every digit of a positive integer is odd.
 * Your method should return true if the number consists entirely of odd digits and false if any of its digits are even.
 * 0, 2, 4, 6, and 8 are even digits, and 1, 3, 5, 7, 9 are odd digits.
 * For example, allDigitsOdd(135319) returns true but allDigitsOdd(9145293) returns false.
 */

import java.util.Scanner;
public class Task11 {
	public static boolean allDigitOdd(int n) {
		boolean flag = true;
		while(n != 0) {
			if((n % 10) % 2 == 0) {
				flag = false;
				break;
			}
			n /= 10;
		}
		return flag;
	}
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		while(sc.hasNext()) {
			System.out.println(allDigitOdd(sc.nextInt()));
		}
	}
}
