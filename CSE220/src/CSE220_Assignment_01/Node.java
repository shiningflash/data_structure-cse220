package CSE220_Assignment_01;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jun 25, 2018
 **/

public class Node {
	public Node next;	// reference of the next Node
	public Integer element;	   // Node element
	
	/**
	 * Default Constructor
	 * @param e --  the given element
	 * @param n --  the given next Node reference
	 * Stores them in Node class
	 */
	public Node(Integer e, Node n) {
		element = e;
		next = n;
	}
}
