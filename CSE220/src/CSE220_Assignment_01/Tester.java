package CSE220_Assignment_01;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jun 25, 2018
 **/

public class Tester {
	public static void main (String[] args)  {
		
		MyOrderList mylist = new MyOrderList();
		
		Node anyNode = new Node(171015, null);
				
		System.out.println("Let's input 19887, 990, 420, 17101, 2013 in the list");
		mylist.insert(new Node(19887, null));
		mylist.insert(anyNode);
		mylist.insert(new Node(990, null));
		mylist.insert(new Node(420, null));
		mylist.insert(new Node(17101, null));
		mylist.insert(new Node(2013, null));
		
		System.out.println("Printing the list (Auto sorted while inserting");
		mylist.showList(); // Output: 420  990  2013  17101  19887  171015  
		
		System.out.println();
		
		System.out.println("cursor :  " + mylist.getCursor().element); // Output: cursor :  420
		System.out.println("next :  " + mylist.gotoNext().element); // next :  990
		System.out.println("prior :  " + mylist.gotoPrior().element); // prior :  420
		
		mylist.retrieve(120); // searchKey :  120 is not found
		mylist.retrieve(171015); // SearchKey :  171015 is found
		
		System.out.println("cursor :  " + mylist.getCursor().element); // cursor :  171015
		
		System.out.println();
		
		mylist.remove();
		System.out.println("After removing cursor : list becomes : ");
		mylist.showList(); // Output: 420  990  2013  17101  19887
		
		mylist.remove(2013);
		System.out.println("After removing 2013 : list becomes : ");
		mylist.showList(); // Output: 420  990  17101  19887
		
	}
}
