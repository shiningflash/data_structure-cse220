package CSE220_Assignment_01;

/**----------------------------------------------------------------------------------**
 **  Implement a MyOrderList ADT (a circular singly-linked-list-based (sorted) list) **
 **----------------------------------------------------------------------------------**/

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    June 25, 2018
 **/

public class MyOrderList {
	
    /**
     * @param cursor -- position identifier to travel the list
     * an specific element in any non-empty list is marked as cursor
     */
	private Node cursor;
	
	/**
	 * Default Constructor
	 * Creates an empty ordered list
	 */
	public MyOrderList() {
		cursor = new Node(null, null);
	}
	
	/**
	 * inserts newElement in its appropriate position within the list
	 * @param newElement -- the key that has to be stored in the list
	 * @param exist --  a boolean type variable to define and check whether the newElement is already exist in list or not
	 * @param gotoBeginning() --  calling gotoBeginning() method to get the first Node of the list
	 * @param gotoBeginning().element --  first element of the list, the smallest of all
	 * @param gotoEnd() --  calling gotoEnd() method to get the last Node of the list
	 * @param gotoEnd().element --  last element of the list, the greatest of all
	 * storing will be in ascending order (next Node element must be greater than the previous one)
	 */
	public void insert(Node newElement) {
		boolean exist = false;
		Node newNode = new Node(newElement.element, null);
		
		// inserting in empty list
		if (cursor.element == null) {
			cursor.element = newElement.element;
			cursor.next = cursor;
			exist = true;	// define exist as true as already stored successfully
		}
		// checking if already exist or not
		// if found, exist bacomes true and stop the checking from there
		else {
			Node n = cursor;
			do {
				if (cursor.element == newElement.element) {
					exist = true;
					break;
				}
				cursor = cursor.next;
			} while (n != cursor);
		}
		
		// newElement does not exist in the list
		// insertion for non-empty  list
		if (!exist) {
			while (true) {
				// if newElement is in the middle range of two serial element, the store it in between them
				if (cursor.element < newElement.element && cursor.next.element > newElement.element) {
					newNode.next = cursor.next;
					cursor.next = newNode;
					break;
				}
				// if newElement is even greater than the last (greatest) element of the list, then store it to the last
				else if(gotoEnd().element < newElement.element) {
					cursor = gotoBeginning();
					newNode.next = cursor;
					cursor = gotoEnd();
					cursor.next = newNode;
					break;
				}
				// if newElement is even smaller than the first (smallest) element of the list, then store it to the beginning 
				else if (gotoBeginning().element > newElement.element) {
					cursor = gotoBeginning();
					newNode.next = cursor;
					cursor = gotoEnd();
					cursor.next = newNode;
					break;
				}
				cursor = cursor.next;	// newElement Node points cursor
			}
		}
		gotoBeginning();	// cursor is in the smallest key
	}

	/**
	 * if list is empty, return null
	 * check which element is most small, and cursor move to it
	 * smallest element indicates the beginning (first) Node as it is ascending sorted list
	 * @return cursor -- the smallest (first) key of non-empty list
	 */
	public Node gotoBeginning() {
		if (isEmpty()) return null;
		else {
			Node smallest = cursor;
			for (Node n = cursor.next; n != cursor; n = n.next) {
				if (n.element < smallest.element) {
					smallest = n;
				}
			}
			cursor = smallest;	// cursor moves to the smallest (first) Node
			return cursor;
		}
	}

	/**
	 * if list is empty, return null
	 * check which element is the greatest, and cursor move to it
	 * greatest element indicates the last Node as it is ascending sorted list
	 * @return cursor -- the greatest (last) key of non-empty list
	 */
	public Node gotoEnd() {
		if (isEmpty()) return null;
		else {
			Node greatest = cursor;
			for (Node n = cursor.next; n != cursor; n = n.next) {
				if (n.element > greatest.element) {
					greatest = n;
				}
			}
			cursor = greatest;	// cursor moves to the greatest (last) Node
			return cursor;
		}
	}

	/**
	 * if first element or cursor element is null, it indicates the list is empty
	 *  non-empty, otherwise
	 * @return a boolean whether it is empty or not
	 */
	public boolean isEmpty() {
		if (cursor.element == null) return true;
		else return false;
	}
	
	/**
	 * searches searchKey in the list
	 * @param searchKey --  the element that have to search from the list
	 * if found, print "SearchKey : #### is not found" and cursor moves to the element
	 * @return the Node that contains searchKey
	 * if not found, print "SearchKey : #### is not found" and return null and cursor does not change
	 */
	public Node retrieve(int searchKey) {
		if (isEmpty()) return null;
		else {
			for (Node n = cursor.next; n != cursor; n = n.next) {
				if (n.element == searchKey) {
					System.out.println("SearchKey :  " + searchKey + " is found");
					cursor = n;
					return cursor;
				}
			}
		}
		System.out.println("searchKey :  " + searchKey + " is not found");
		return null;
	}
	
	/**
	 * Removes the element marked by the cursor
	 * @param gotoPrior() --  returns the Node that just before cursor
	 * cursor moves the next Node from the deleted one
	 * if the deleted element was in the end of the list, then moves the cursor to the beginning of the list
	 * @return updated cursor
	 */
	public Node remove() {
		if (isEmpty()) return null;
		else {
			Node n = cursor;
			gotoPrior().next = n.next;
			cursor = n.next;
			return cursor;
		}
	}
	
	/**
	 * Removes the element from a list contains the deleteKey
	 * cursor moves the next Node from the deleted one
	 * if the deleted element was in the end of the list, then moves the cursor to the beginning of the list
	 * @param retrieve(deleteKey) --  returns the cursor which element is simillar to deleteKey
	 * @param deleteKey --  the element that have to be deleted from the list
	 * @return updated cursor
	 */
	public Node remove(Integer deleteKey) {
		if (isEmpty()) return null;
		else {
			cursor = retrieve(deleteKey);
			remove();
			return cursor;
		}
	}
	
	/**
	 * for empty list, return null
	 * otherwise,
	 * @return updated cursor (Node before previous cursor)
	 */
	public Node gotoPrior() {
		if (isEmpty()) return null;
		else {
			Node n;
			for (n = cursor.next; n.next != cursor; n = n.next);
			cursor = n;
			return cursor;
		}
	}
	
	/**
	 * for empty list, return null
	 * otherwise,
	 * @return updated cursor (Node after previous cursor)
	 */
	public Node gotoNext() {
		if (isEmpty()) return null;
		cursor = cursor.next;
		return cursor;
	}
	
	/**
	 * for empty list, return null
	 * and for non-empty,
	 * @return the Node marked by the cursor
	 */
	public Node getCursor() {
		if (isEmpty()) return null;
		else return cursor;
	}
	
	/**
	 * Removes all element from the list
	 * simply, cursor changed to null
	 */
	public void clear() {
		cursor = new Node(null, null);
	}
	
	/**
	 * if list is empty, Prints "List Empty"
	 * Prints all element from the beginning to end otherwise
	 */
	public void showList() {
		if (isEmpty()) System.out.println("List Empty");
		else {
			gotoBeginning();
			Node n = cursor;
			do {
				System.out.print(n.element + "  ");
				n = n.next;
			} while (n != cursor);
		}
		System.out.println();
	}
}

