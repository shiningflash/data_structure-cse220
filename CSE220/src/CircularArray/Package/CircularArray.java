package CircularArray.Package;

import java.util.*;
public class CircularArray {
//	private int array[]; // queue array
//	private int capacity;
//	private int rear;	// last index
//	private int front;	// first index
//	
//	public CircularArray() {
//		capacity = 5;
//		array = new int[size];
//		rear = 0;
//		front = 0;
//	}
//	
//	public CircularArray(int capacity) {
//		this.capacity = capacity;
//		array = new int[this.capacity];
//		rear = 0;
//		front = 0;
//	}
//	
//	public boolean isEmpty() {
//		return front == rear;
//	}
//	
//	public boolean isFull() {
//		return capacity == size();
//	}
//	
//	public int size() {
//		return (capacity - front + rear) % capacity;
//	}
//	
//	public boolean enqueue(int item) {
//		if (isFull()) return false;
//		else {
//			total++;
//			array[rear++] = item;
//			return true;
//		}
//	}
//	
//	public int dequeue() {
//		int item = 0;
//		if (!isEmpty()) {
//			total--;
//			item = array[front];
//			front = (front + 1) % size;
//		}
//		return item;
//	}
//	
//	public void showAll() {
//		if (!isEmpty()) {
//			int f = front;
//			for (int i = front; i < size; i++) {
//				System.out.println(array[f]);
//				f = (f + 1) % size;
//			}
//		}
//	}
//	
//	public static void main (String[] args) {
//		CircularArray q = new CircularArray();
//		q.enqueue(12231);
//		q.enqueue(3445);
//		q.enqueue(4536);
//		q.enqueue(4536787);
//		q.enqueue(4536477);
//		q.enqueue(6578980);
//		q.showAll();
//		
//		System.out.println();
//		
//		q.dequeue();
//		q.dequeue();
//		q.showAll();
//	}
	
	private int array[];
	private int capacity;
	private int rear;
	private int front;
	
	public CircularArray() {
		capacity = 5;
		array = new int[capacity];
		rear = 0; front = 0;
	}
	
	public CircularArray(int capacity) {
		this.capacity = capacity;
		array = new int[this.capacity];
		rear = 0; front = 0;
	}
	
	public boolean isEmpty() {
		return rear == front;
	}
	
	public boolean isFull() {
		return capacity == size();
	}
	
	public int size() {
		return (capacity - rear + front) % capacity;
	}
	
	public boolean enqueue(int item) {
		if (isFull()) return false;
		else {
			total++;
			array[rear++] = item;
			return true;
		}
	}
	
	public static void main (String args[]) {
		
	}
}


// method : constructor, isEmpty(), enqueue(), dequeue(), showAll(), isFull()
// variable : array[], size, rear, front, total