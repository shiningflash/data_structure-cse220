package CircularArray.Package;

import java.util.*;
public class Queue {
	private int array[]; // queue array
	private int size;	// size of array
	private int total;	// index checking, isfull()
	private int rear;	// last index
	private int front;	// first index
	
	public Queue() {
		size = 5;
		array = new int[size];
		total = 0;
		rear = 0;
		front = 0;
	}
	
	public Queue(int size) {
		this.size = size;
		array = new int[this.size];
		total = 0;
		rear = 0;
		front = 0;
	}
	
	public boolean isEmpty() {
		return total == 0;
	}
	
	public boolean isFull() {
		return total == size;
	}
	
	public boolean enqueue(int item) {
		if (isFull()) return false;
		else {
			total++;
			array[rear++] = item;
			return true;
		}
	}
	
	public int dequeue() {
		int item = 0;
		if (!isEmpty()) {
			total--;
			item = array[front];
			front = (front + 1) % size;
		}
		return item;
	}
	
	public void showAll() {
		if (!isEmpty()) {
			int f = front;
			for (int i = front; i < size; i++) {
				System.out.println(array[f]);
				f = (f + 1) % size;
			}
		}
	}
	
	public static void main (String[] args) {
		Queue q = new Queue();
		q.enqueue(12231);
		q.enqueue(3445);
		q.enqueue(4536);
		q.enqueue(4536787);
		q.enqueue(4536477);
		q.enqueue(6578980);
		q.showAll();
		
		System.out.println();
		
		q.dequeue();
		q.dequeue();
		q.showAll();
	}
}


// method : constructor, isEmpty(), enqueue(), dequeue(), showAll(), isFull()
// variable : array[], size, rear, front, total