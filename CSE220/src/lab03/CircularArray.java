package lab03;

import java.util.Arrays;

/**
 * @author  Amirul Islam Al Mamun
 * @id      17101537
 * @date    May 13, 2018
 **/

public class CircularArray {

	  private int start;
	  private int size;
	  private Object [] cir;
	  
	  /*
	   * if Object [] lin = {10, 20, 30, 40, null}
	   * then, CircularArray(lin, 2, 4) will generate
	   * Object [] cir = {40, null, 10, 20, 30}
	   */
	  public CircularArray(Object [] lin, int st, int sz) {
		  size = sz; start = st;	// assign global size and start value
		  cir = new Object[lin.length];	// created new circular array
		  for (int i = 0; i < size; i++)
			  cir[((st++)) % (cir.length)] = lin[i];	// assign value in circulararray from lineararray
	  }
	  
	  //Prints from index --> 0 to cir.length-1
	  public void printFullLinear() {
	      for (int i = 0; i < cir.length; i++)
	    	  System.out.print(cir[i] + ((i < cir.length-1) ? ", " : ".\n"));
	  }
	  
	  // Starts Printing from index start. Prints a total of size elements
	  public void printForward(){
	    for (int i = 0, j = start; i < size; i++, j = (j++) % cir.length)
	    	System.out.print(cir[j] + ((i < size-1) ? ", " : ".\n"));
	  }
	  
	  
	  public void printBackward(){
	    for (int i = 0, j = size + start -1; i < size; i++) {
	    	System.out.print(cir[j % cir.length] + ((i < size-1) ? ", " : ".\n"));
	    	j = (j-1 > 0 ? --j : cir.length-1);
	    }
	  }
	  
	  // With no null cells
	  public void linearize(){
	    Object temp[] = new Object[size];
	    for (int i = 0, st = start; i < size; i++, st = (st+1) % cir.length)
	    		temp[i] = cir[st];
		cir = temp;
	  }
	  
	  // Do not change the Start index
	  public void resizeStartUnchanged(int newcapacity){
	    int len = cir.length;
	    Object temp[] = new Object[newcapacity];
	    for (int i = 0, st = start, j = start; i < size; i++, st = (st+1) % temp.length, j = (j+1) % cir.length)
	    		temp[st] = cir[j];
	    cir = temp;
	  }
	  
	  // Start index becomes zero
	  public void resizeByLinearize(int newcapacity){
		Object temp[] = new Object[newcapacity];
		for (int i = 0, j = start; i < cir.length; i++, j = (j+1) % cir.length)
			temp[i] = cir[j];
		cir = temp;
		start = 0;
	  }
	  
	  /* pos --> position relative to start. Valid range of pos--> 0 to size.
	   * Increase array length by 3 if size==cir.length
	   * use resizeStartUnchanged() for resizing.
	   */
	  public void insertByRightShift(Object elem, int pos){
		  if (pos < 0 || pos > size) System.out.println("Inalid Position");
		  else if (size == cir.length) resizeStartUnchanged(cir.length+3);
		  if (pos >= 0 || pos <= size) {
			  if (cir[(start+pos) % cir.length] == null) cir[(start+pos) % cir.length] = elem;
			  else {
				  for (int i = 0, k = (start+size-1) % cir.length; i < size - pos; i++, k--) {
					  if (k-1 < 0) k = cir.length-1;
					  cir[(k+1) % cir.length] = cir[k];
				  }
				  cir[(start + pos) % cir.length] = elem;
				  size++;
			  }
		  }
	  }
	  
	  public void insertByLeftShift(Object elem, int pos){
		  if (pos < 0 || pos > size) System.out.println("Inalid Position");
		  else if (size == cir.length) resizeStartUnchanged(cir.length+3);
		  if (pos >= 0 || pos <= size) {
			  for (int i = 0, j = ((start-1) < 0 ? cir.length-1 : start-1); i < start-pos; i++, j = (j+1) % cir.length)
				  cir[j] = cir[j+1];
			  cir[(start+pos) % cir.length] = elem;
			  start = ((start-1) < 0) ? cir.length : --start;
			  size++;
		  }
	  }
	  
	  /* parameter--> pos. pos --> position relative to start.
	   * Valid range of pos--> 0 to size-1
	   */
	  public void removeByLeftShift(int pos){
	    if (pos  < 0  || pos > size) System.out.println("Invalid Position");
	    else {
	    	for (int i = 0, j = (start+pos) % cir.length; i < (size-pos-1); i++, j = (j+1) % cir.length)
	    		cir[j] = cir[(j+1) % cir.length];
	    	cir[(start+(--size)) % cir.length] = null;
	    }
	  }
	  
	  /* parameter--> pos. pos --> position relative to start.
	   * Valid range of pos--> 0 to size-1
	   */
	  public void removeByRightShift(int pos){
		  if (pos  < 0  || pos > size) System.out.println("Invalid Position");
		    else {
		    	for (int i = 0, j = (start+pos) % cir.length; i <= (size-pos); i++, j = ((j-1) > 0 ? --j : cir.length-1))
		    		cir[j] = cir[((j-1)  < 0 ? cir.length-1 : j-1)];
		    	cir[start] = null;
		    	size--;
		    	start = (start+1) % cir.length;
		    }
	  }
	  
	  
	  //This method will check whether the array is palindrome or not
	  public void palindromeCheck(){
	    //TO DO
	  }
	  
	  
	  //This method will sort the values by keeping the start unchanged
	  public void sort(){
		  //TO DO
	  }
	  
	  //This method will check the given array across the base array and if they are equivalent interms of values return true, or else return false
	  public boolean equivalent(CircularArray k){
		  //TO DO
		  return false; // Remove this line
	  }
	
}