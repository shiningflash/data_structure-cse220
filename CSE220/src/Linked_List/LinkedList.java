package Linked_List;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 22, 2018
 **/


public class LinkedList {
	
	private Node head;
	
	// nested class/ child class
	public class Node {
		private int value;	// store a value
		private Node link;	// store address of that Node
	}
	
	// constructor
	public LinkedList(int item) {
		head = new Node();
		head.value = item;
		head.link = null;
	}
	
	//insert a value
	public boolean insertValue(int item) {
		Node n = new Node();
		n.value = item;
		n.link = head;
		head = n;
		return true;
	}
	
	// print whole list
	public void printList() {
		for (Node z = head; z != null; z = z.link) {
			System.out.print(z.value + " ");
		}
	}
	
	// sort in assending order
	public void sortList() {
		int temp = 0;
		for (Node a = head; a.link != null; a = a.link) {
			for (Node b = head; b.link != null; b = b.link) {
				if(b.value > b.link.value) {
					temp = b.value;
					b.value = b.link.value;
					b.link.value = temp;
				}
			}
		}
	}
	
	// delete a particular item from the list if it exist
	// if it is not exist, then return false;
	public boolean deleteItem(int item) {
		if (head.value == item) {
			head = head.link;
			return true;
		} else {
			Node x = head;
			Node y = head.link;
			while (true) {
				if (y == null || y.value == item) break;
				else {
					x = y;
					y = y.link;
				}
			}
			if (y != null) {
				x.link = y.link;
				return true;
			} else return false;
		}
	}
}
