package Linked_List;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 22, 2018
 **/


import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		
		LinkedList l = new LinkedList(2);
		
		l.insertValue(89);
		l.insertValue(234);
		l.printList();
		
		System.out.println();
		l.deleteItem(89);
		l.printList();
		
		System.out.println();
		l.deleteItem(0);
		l.deleteItem(234);
		l.printList();
		
		System.out.println();
		l.insertValue(785);
		l.insertValue(8565);
		l.insertValue(5);
		l.insertValue(7853768);
		l.insertValue(000);
		l.insertValue(1);
		l.insertValue(2);
		l.insertValue(785234545);
		l.printList();
		
		System.out.println();
		l.sortList();
		l.printList();
		
		
		/*
		 * Output:
		 * 
		 * 234 89 2 
		 * 234 2 
		 * 2 
		 * 785234545 2 1 0 7853768 5 8565 785 2 
		 * 0 1 2 2 5 785 8565 7853768 785234545 
		 */
	}
}
