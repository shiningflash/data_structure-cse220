
/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 7, 2018
 **/
import java.util.Scanner;
public class Expression_Checker {

	public static class Stack {

		int size;
		char Stack[];
		int top;

		// unused
		public Stack() {
			size = 10;
			Stack = new char[size];
			top = -1;
		} 

		public Stack(int sz) {
			size = sz;
			Stack = new char[size];
			top = -1;
		} 

		public boolean push(char ch) {
			if (!isFull()) {
				top++;
				Stack[top] = ch;
				return true;
			}
			return false;
		}

		public char pop() {
			return Stack[top--];
		}

		// unused
		public char peek() {
			return Stack[top];
		}

		public boolean isEmpty() {
			return top == -1;
		}

		// unused
		public boolean isFull() {
			return top == size-1;
		}
		
		//unused
		public int top() {
			return top;
		}

		// unused
		public void printStack() {
			for (int i = 0; i <= top; i++) {
				System.out.print(Stack[i]);
			}
			System.out.println();
		}
	}

	public static boolean Parenthesis_Matched(String str) {
		Stack p = new Stack(str.length());	// goto 2nd constructor
		boolean flag = false;	// string contains no expression
		for (int k = 0; k < str.length(); k++) {
			//	opening parenthesis - push it onto the stack
			if (str.charAt(k) == '[' || str.charAt(k) == '{' || str.charAt(k) == '(') {
				p.push(str.charAt(k));
				flag = true;	// string contains expression
			}
			//	 closing parenthesis - if match - remove top
			if (str.charAt(k) == ']' || str.charAt(k) == '}' || str.charAt(k) == ')') {
				if (p.isEmpty()) return false;
				else if (!maching_pair(p.pop(), str.charAt(k))) {
					return false;
				}
			}
		}

		if (p.isEmpty() && flag) return true;
		else return false;
	}

	// matching top value with string index value
	private static boolean maching_pair(char a, char b) {
		if (a == '(' && b == ')') return true;
		else if (a == '{' && b == '}') return true;
		else if (a == '[' && b == ']') return true;
		return false;
	}

	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			String str = sc.next();
			System.out.println(Parenthesis_Matched(str));
		}
	}
}