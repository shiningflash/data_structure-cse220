package lab06_stack;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 7, 2018
 **/

public class StackOverflowException extends Exception{
	public String toString() {
		return "StackOverflowException\n";
	}
}