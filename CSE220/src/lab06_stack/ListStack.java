package lab06_stack;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 7, 2018
 **/
public class ListStack implements Stack{
    int size;
    Node top;
  
    
    public ListStack(){
        size = 0;
        top = null;
    }
    
    public int size() {
        return size;
    }
    public boolean isEmpty() {
        return (top == null);
    }
    public void push(Object e) throws StackOverflowException {
        Node old = top;
        top = new Node(null, null);
        top.val = e;
        top.next = old;
        size++;
    }
	public Object pop() throws StackUnderflowException {
        if (isEmpty()) throw new StackUnderflowException();
        Object value = top.val;
        top = top.next;
        size--;
        return value;
    }
    public Object peek() throws StackUnderflowException {
        if (isEmpty()) throw new StackUnderflowException();
        else return top.val;
    }
    public String toString() {
        StringBuilder s = new StringBuilder();
        Node k = top;
        for (int i = 0; i < size; i++) {
        	s.append(k.val + " ");
        	k = k.next;
        }
        return s.toString();
    }
    public Object[] toArray() {
    	Node k = top;
        Object arr[] = new Object[size];
        for (int i = 0; i < size; i++) {
            arr[i] = k.val;
            k = k.next;
        }
        return arr;
    }
    
    public int search(Object e) {
    	Node k = top;
        for (int i = 0; i < size; i++) {
            if (k.val == e) return i;
            k = k.next;
        }
        return -1;
    }
}