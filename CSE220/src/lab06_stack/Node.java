package lab06_stack;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 7, 2018
 **/

public class Node{
    Object val;
    Node next;
    
    public Node(Object v, Node n){
        val = v;
        next = n;
    }
}