package lab06_stack;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 7, 2018
 **/

public class ArrayStack implements Stack{
    int size;
    Object [] data;
    
    public ArrayStack(){
        size=-1;
        data = new Object[5];
    }  
    public int size() {
        return size+1;
    }
    public boolean isEmpty() {
        return (size == -1);
    }
    public void push(Object e) throws StackOverflowException {
        if (isFull()) throw new StackOverflowException();
        else {
            size++;
            data[size] = e;
        }
    }
    private boolean isFull() {
		return size == data.length-1;
	}
	public Object pop() throws StackUnderflowException {
        if (isEmpty()) throw new StackUnderflowException();
        else return data[size--];
    }
    public Object peek() throws StackUnderflowException {
        if (isEmpty()) throw new StackUnderflowException();
        else return data[size];
    }
    public String toString() {
    	if (isEmpty()) return "Empty Stack";
        StringBuilder s = new StringBuilder();
        for (int i = size; i >= 0; i--) {
        	s.append(data[i] + " ");
        }
        return s.toString();
    }
    public Object[] toArray() {
    	int sz = size;
        Object arr[] = new Object[size+1];
        for (int i = 0; i <= size; i++) {
            arr[i] = data[sz];
            sz--;
        }
        return arr;
    }
    
    public int search(Object e) {
        for (int i = 0; i <= size; i++) {
            if (data[i] == e) return size-i;
        }
        return -1;
    }
}