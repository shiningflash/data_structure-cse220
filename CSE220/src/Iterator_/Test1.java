package Iterator_;

//Java program to demonstrate Enumeration
import java.util.Enumeration;
import java.util.Vector;
import java.util.Iterator;
import java.util.ArrayList;
import java.lang.*;

public class Test1
{
    public static void main(String[] args)
	{
    	 // creating an Integer vector
		 Vector<Integer> v = new Vector<Integer>();
		 v.addElement(723685);	// add element
		 v.addElement(562776);
		 v.addElement(736247);
		 System.out.println(v);
		 System.out.println(v.size());	// print size
		 System.out.println(v.capacity());	// capacity by Default 10
//		 v.setSize(20);	 // size increased to 20  
//		 System.out.println(v.size());	// print updated size
		 
		 // creating new String vector
		 Vector<String> vec = new Vector<String>();
		 vec.addElement("Amirul");
		 vec.addElement("Sumon");
		 System.out.println(vec);	// print toString
		 System.out.println(vec.size());	// print vector size
		 System.out.println(vec.capacity());	// print vector capacity
		 
		 
		 // Enumeration : one kind of Iterator
		 Enumeration it = vec.elements();
		 while (it.hasMoreElements())
			 System.out.println(it.nextElement());	// print using Enumeration
		 
		 for (int i = 0; i < vec.size(); i++) 
			 System.out.println(vec.get(i));	// print by get() method
		 
		 // add elements further at vec
		 vec.addElement("Kabbya");
		 vec.addElement("Labbnnya");
		 vec.addElement("Samiul");
		 vec.setElementAt("Mamun", 0);	// replace 0 index "Amirul" by "Mamun"
		 System.out.println(vec);
		 System.out.println(vec.size());
		 System.out.println(vec.capacity());
		 
		 System.out.println(vec.contains("afia"));	// element presence checking
		 try {
			 System.out.println(vec.get(12));	// print element of specified index
		 } catch(ArrayIndexOutOfBoundsException e) {
			 System.out.println(e);
		 }
		 System.out.println(vec.firstElement());	// print first element
		 System.out.println(vec.lastElement());		// print last element
		 System.out.println(vec.isEmpty());		// vec is not empty
		 vec.clear();
		 System.out.println(vec.isEmpty());		// vec is empty now
		 
		 // again vector v ; vector to ArrayList conversion
		 ArrayList<Integer> l = new ArrayList<Integer>(v);
		 for (int i : l)
			 System.out.print(i + " ");
		 System.out.println();
		 v.sort(null);	// vector sorting
		 System.out.println(v);		// print sorted vector toString
		 
		 // copy element to vector from arraylist
		 Vector copyAL = new Vector();
		 copyAL.addAll(l);
		 
		 // copy element to vector from array
		 int ara[] = {12, 34, 45, 3};
		 Vector copyA = new Vector();
		 copyA.add(ara);
		 
		// another vector containing String Integer both
		Vector vc = new Vector();
		vc.add(765326);		// add int
		vc.add("Amirul");	//add String
		vc.add(757);
		System.out.println(vc);
		System.out.println(vc.size());
		System.out.println(vc.capacity()); 
    }
}