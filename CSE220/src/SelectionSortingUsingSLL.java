/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 20, 2018
 **/

public class SelectionSortingUsingSLL {
	
	public static class Node {
		public int value;
		public Node next;
		
		public Node(int a, Node n){
		    value = a;
		    next = n; 
		}
	}
	
	public static class LinkedList {
		public Node head;
		public LinkedList(int[] a) {
		    head = new Node(a[0], null);    // set head by first element of object[]
		    Node tail = head;
		    for (int i = 1 ; i < a.length; i++){
		      Node x = new Node(a[i], null);    // set i'th node by i'th element of object[]
		      tail.next = x;
		      tail = x; // tail = tail.next
		    }
		}
		
		public void sortList() {
			Node a, b, min;
			for (a = head; a != null; a = a.next) {
				min = a;
				for (b = a.next; b != null; b = b.next) {
					if (b.value < min.value) {
						min = b;
					}
				}
				int temp = a.value;
				a.value = min.value;
				min.value = temp;
			}
		}
		public void printList() {
			for (Node n = head; n != null; n = n.next) {
				System.out.print(n.value + " ");
			}
			System.out.println();
		}
		
		public static void main (String[] args) {
			int a[] = {5, 2343, 76, 0, 1, 4544};
			LinkedList l = new LinkedList(a);
			l.sortList();
			l.printList();
		}
	}
}
