package assignment_2b;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 17, 2018
 **/

import java.util.Scanner;
import java.util.Stack;//you should use the stack you have built in assignment 03. If you are using your own stack, remove this line; otherwise, import & use java's default stack class.
public class Assignment_2b {
	public static String postFixBuilder(String s){
		char []a=s.toCharArray();
		Stack<Character> st= new Stack<Character>();    //Hello dear sir, I used only one stack to build post fix, I guess the code got lengthy-Asif
		String pfb="";


		for(int i=0;i<a.length;i++){

			if ((int)a[i]>47 && (int)a[i]<58 ){
				pfb=pfb+a[i];
			}
			else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.isEmpty()) ){
				st.push(a[i]);
			}

			else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
				st.push(a[i]);
			}

			else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
				st.push('b');
				i++;
				while(a[i]!=')' || a[i]!='}' || a[i]!=']'){

					if( a[i]==')' || a[i]=='}' || a[i]==']' ){
						break; 
					}
					else if ((int)a[i]>47 && (int)a[i]<58 ){
						pfb=pfb+a[i];
					}
					else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.peek()=='b') ){
						st.push(a[i]);
					}

					else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
						st.push(a[i]);
					}

					else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
						st.push('b');
						i++;
						while(a[i]!=')' || a[i]!='}' || a[i]!=']'){

							if( a[i]==')' || a[i]=='}' || a[i]==']' ){
								break; 
							}
							else if ((int)a[i]>47 && (int)a[i]<58 ){
								pfb=pfb+a[i];
							}
							else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.peek()=='b') ){
								st.push(a[i]);
							}

							else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
								st.push(a[i]);
							}

							else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
								st.push('b');
								i++;
								while(a[i]!=')' || a[i]!='}' || a[i]!=']'){

									if( a[i]==')' || a[i]=='}' || a[i]==']' ){
										break; 
									}
									else if ((int)a[i]>47 && (int)a[i]<58 ){
										pfb=pfb+a[i];
									}
									else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.peek()=='b') ){
										st.push(a[i]);
									}

									else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
										st.push(a[i]);
									}

									else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
										st.push('b');
										i++;
										while(a[i]!=')' || a[i]!='}' || a[i]!=']'){

											if( a[i]==')' || a[i]=='}' || a[i]==']' ){
												break; 
											}
											else if ((int)a[i]>47 && (int)a[i]<58 ){
												pfb=pfb+a[i];
											}
											else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.peek()=='b') ){
												st.push(a[i]);
											}

											else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
												st.push(a[i]);
											}

											else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
												st.push('b');
												i++;
												while(a[i]!=')' || a[i]!='}' || a[i]!=']'){

													if( a[i]==')' || a[i]=='}' || a[i]==']' ){
														break; 
													}
													else if ((int)a[i]>47 && (int)a[i]<58 ){
														pfb=pfb+a[i];
													}
													else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.peek()=='b') ){
														st.push(a[i]);
													}

													else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
														st.push(a[i]);
													}

													else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
														st.push('b');
														i++;
														while(a[i]!=')' || a[i]!='}' || a[i]!=']'){

															if( a[i]==')' || a[i]=='}' || a[i]==']' ){
																break; 
															}
															else if ((int)a[i]>47 && (int)a[i]<58 ){
																pfb=pfb+a[i];
															}
															else if( (a[i]== '+' || a[i] == '-' || a[i] == '*' || a[i] == '/' || a[i] == '%' ) && (st.peek()=='b') ){
																st.push(a[i]);
															}

															else if( ( a[i]=='%' || a[i]=='/' || a[i]=='*' ) && ( st.peek()=='+' || st.peek()=='-' )){
																st.push(a[i]);
															}

															//            else if( a[i]=='(' || a[i]=='{' || a[i]=='[' ){
															//            
															//            } 

															else{
																pfb=pfb+st.pop();
																st.push(a[i]);
															}

															i++;

														}
														while(st.peek()!='b'){
															pfb=pfb+st.pop();
														}
														st.pop();
													} 

													else{
														pfb=pfb+st.pop();
														st.push(a[i]);
													}

													i++;

												}
												while(st.peek()!='b'){
													pfb=pfb+st.pop();
												}
												st.pop();
											} 

											else{
												pfb=pfb+st.pop();
												st.push(a[i]);
											}

											i++;

										}
										while(st.peek()!='b'){
											pfb=pfb+st.pop();
										}
										st.pop();
									} 

									else{
										pfb=pfb+st.pop();
										st.push(a[i]);
									}

									i++;

								}
								while(st.peek()!='b'){
									pfb=pfb+st.pop();
								}
								st.pop();
							} 

							else{
								pfb=pfb+st.pop();
								st.push(a[i]);
							}

							i++;

						}
						while(st.peek()!='b'){
							pfb=pfb+st.pop();
						}
						st.pop();
					} 

					else{
						pfb=pfb+st.pop();
						st.push(a[i]);
					}

					i++;

				}
				while(st.peek()!='b'){
					pfb=pfb+st.pop();
				}
				st.pop();
			} 


			else{
				pfb=pfb+st.pop();
				st.push(a[i]);
			}



		}
		while(!st.isEmpty()){
			pfb=pfb+st.pop();
		}
		return pfb;
	}



	public static int postFixEvaluator(String s){
		Stack<Integer> sta=new Stack<Integer>();
		for(int i=0;i<s.length();i++){
			if((int)s.charAt(i)>47 && (int)s.charAt(i)<58){
				sta.push((int)s.charAt(i)-48);
			}
			else if(s.charAt(i)=='+'){
				int y=sta.pop();
				int x=sta.pop();
				sta.push(x+y);
			}
			else if(s.charAt(i)=='-'){
				int y=sta.pop();
				int x=sta.pop();
				sta.push(x-y);
			}
			else if(s.charAt(i)=='*'){
				int y=sta.pop();
				int x=sta.pop();
				sta.push(x*y);
			}
			else if(s.charAt(i)=='/'){
				int y=sta.pop();
				int x=sta.pop();
				sta.push(x/y);
			}
			else if(s.charAt(i)=='%'){
				int y=sta.pop();
				int x=sta.pop();
				sta.push(x%y);
			}

		}
		return sta.pop();
	}
	public static void main(String[]args){
		String exp=new Scanner(System.in).next();
		String postFixExp = postFixBuilder(exp);
		System.out.println("Post Fix Expression: "+postFixExp);
		int result = postFixEvaluator(postFixExp);
		System.out.println("Answer: "+result);
	}
}
//2/3+[7+{3-(4/3)+4}+8]*9