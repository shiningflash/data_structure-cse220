package data_structure;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

import java.util.*;
public class Main {
	public static void main(String[] args) {
//		Scanner sc = new Scanner(System.in);
//		
		IntStack Stack = new IntStack();
//		if(!Stack.isFull()) {
//			for (int i = 0; i < Stack.size(); i++) {
//				Stack.push(sc.nextInt());
//			}
//		}
//		System.out.println(Stack);
//		System.out.println(Stack.pop());
//		System.out.println(Stack.pop());
//		System.out.println(Stack.top());
//		System.out.println(Stack.pop());
//		System.out.println(Stack.top());
//		System.out.println(Stack.size());
//		
		Stack.push(999);
		Stack.push(888);
		Stack.push(222);
//		
		System.out.println(Stack.pop());
		System.out.println(Stack.pop());
		System.out.println(Stack.top());
//		System.out.println(Stack.pop());
//		System.out.println(Stack.top());
		System.out.println(Stack.size());
		
//		IntStack Stack1 = new IntStack(5);
//		
//		for (int i = 0; i < Stack1.size(); i++) {
//			Stack1.push(sc.nextInt());
//		}
//		System.out.println(Stack1.pop());
//		System.out.println(Stack1.pop());
//		System.out.println(Stack1.top());
//		System.out.println(Stack1.pop());
//		System.out.println(Stack1.top());
//		System.out.println(Stack1.size());
//		System.out.println(Stack1);
//		
//		int num[] = {99, -11, 0, 11};
//		String nam[] = {"Sakib", "Zahin", "Amirul"};
//		Arrays.sort(num);
//		Arrays.sort(nam);
//		System.out.println(Arrays.toString(num) + "\n" + Arrays.toString(nam));
		
		person person1 = new person("Amirul", 1710);
		person person2 = new person("Samiul", 1721);
		personStack stack2 = new personStack();
		
		stack2.push(person1);
		stack2.push(person2);
		
		System.out.println(stack2.pop().toString());
		System.out.println(stack2.pop().toString());
		
		
		/*
		 * Output:
		 * 
		 * 222
		 * 888
		 * 999
		 * 5
		 * name: Samiul
		 * roll no: 1721
		 * name: Amirul
		 * roll no: 1710
		 */
	}
}
