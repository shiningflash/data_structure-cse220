package data_structure;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

public class personStack {
	private person stack[];
	private int top;
	private int size;
	
	public personStack() {
		top = -1;
		size = 5;
		stack = new person[5];
	}
	
	public personStack(int size) {
		top = -1;
		this.size = size;
		stack = new person[this.size];
	}
	
	public boolean isFull() {
		if (top == size-1) return true;
		else return false;
	}
	
	public boolean push(person item) {
		if(!isFull()) {
			top++;
			stack[top] = item;
			return true;
		} else return false;
	}
	
	public person pop() {
		return stack[top--];
	}
	
	public boolean isEmpty() {
		return (top == -1);
	}
	
	public int size() {
		return size;
	}
	
	public person top() {
		return stack[top];
	}
}
