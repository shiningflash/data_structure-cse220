package data_structure;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

public class person {
	public String name;
	public int roll;
	public person(String name, int roll) {
		this.name = name;
		this.roll = roll;
	}
	
	public String toString() {
		return "name: " + name + "\nroll no: " + roll;
	}
}
