package data_structure;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

public class IntStack {
	private int stack[];
	private int top;
	private int size;
	
	public IntStack() {
		top = -1;
		size = 5;
		stack = new int[size];
	}
	
	public IntStack(int size) {
		top = -1;
		this.size = size;
		stack = new int[this.size];
	}
	
	public boolean isFull() {
		if (top == size-1) return true;
		else return false;
	}
	
	public boolean push(int item) {
		if(!isFull()) {
			top++;
			stack[top] = item;
			return true;
		} else return false;
	}
	
	public int pop() {
		return stack[top--];
	}
	
	public boolean isEmpty() {
		return (top == -1);
	}
	
	public int size() {
		return size;
	}
	
	public int top() {
		return stack[top];
	}
}
