package lab01_part02;

// task07

public class BankAccount {
	public String name;
	public String address;
	public String accountID;
	public double balance;
	
	public BankAccount() {
		name = "Bagdad ahmed";
		address = "Cox's Bazar";
		accountID = "17305640";
		balance = 5000000;
	}
	public String getName() {
		return name;
	}
	public void setName(String n) {
		name = n;
	}
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String i) {
		accountID = i;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String a) {
		address = a;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double c) {
		balance = c;;
	}
	public void addInterest() {
		balance = balance + ( (7*balance) / 100);
	}
	
	public static void main (String[] args) {
		BankAccount acc1 = new BankAccount();
		BankAccount acc2 = new BankAccount();
		BankAccount acc3 = new BankAccount();
		System.out.println(acc1.getName());
		System.out.println(acc1.getAddress());
		System.out.println(acc1.getBalance());
		System.out.println(acc1.getAccountID());
		acc1.addInterest();
		System.out.println(acc1.getBalance());
		
		System.out.println();
		
		acc2.setName("Amirul Islam");
		acc2.setAddress("Mohakhali");
		acc2.setAccountID("17101537");
		acc2.setBalance(2000000);
		System.out.println(acc2.getName());
		System.out.println(acc2.getAddress());
		System.out.println(acc2.getBalance());
		System.out.println(acc2.getAccountID());
		acc2.addInterest();
		System.out.println(acc2.getBalance());
		
		System.out.println();
		
		System.out.println(acc1.getName());
		System.out.println(acc1.getAddress());
		System.out.println(acc1.getBalance());
		System.out.println(acc1.getAccountID());
		System.out.println(acc1.getBalance());
		
		System.out.println();
		
		acc3 = acc2;
		System.out.println(acc3.getName());
		System.out.println(acc3.getAddress());
		System.out.println(acc3.getBalance());
		System.out.println(acc3.getAccountID());
		System.out.println(acc3.getBalance());
		
	}
}
