package lab01_part02;

import java.util.*;
import java.lang.*;

public class exceptionPractice {
	static void method() throws ClassNotFoundException {
		try {
			String s = null;
			System.out.println(s.length());
		} catch(NullPointerException ne) {
			System.out.println(ne);
			throw new ClassNotFoundException();
		}
	}
	public static void main(String[] args) {
		try {
			method();
		} catch(ClassNotFoundException ce) {
			System.out.println(ce);
		} finally {
			System.out.println("Finished");
		}
	}
}