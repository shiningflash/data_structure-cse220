package lab01_part02;

import java.util.Scanner;
import java.util.Arrays;
public class task10 {
	public static int[] rightRotate(int[] array, int n) {
		int len = array.length;
		int rightRotate[] = new int[len];
		for (int i = 0; i < len; i++) {
			rightRotate[i] = array[(i+n) % len];
		}
		return rightRotate;
	}
	public static int[] leftRotate(int[] array, int n) {
		int len = array.length;
		int leftRotate[] = new int[len];
		for (int i = 0; i < len; i++) {
			leftRotate[i] = array[(i+len-n) % len];
		}
		return leftRotate;
	}
	public static void  main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int array[] = {1, 4, 8, 16, 25, 36, 49, 64, 81, 100};
		System.out.print("Enter position to rotate: ");
		int n = sc.nextInt();
		int rightRotate[] = rightRotate(array, n);
		int leftRotate[] = leftRotate(array, n);
		System.out.println("Right Rotate (" + n + "): " + Arrays.toString(rightRotate));
		System.out.println("Left Rotate (" + n + "): " + Arrays.toString(leftRotate));
	}
}
