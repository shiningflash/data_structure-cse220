package lab01_part02;

public class ComplexNumber extends RealNumber {
	public double imaginaryValue = 1.0;
	public ComplexNumber(double a, double b) {
		super(a);
		imaginaryValue = b;
	}

	public ComplexNumber() {
		super(1.0);
	}
	
	public String toString() {
        return super.toString() + "\nImaginaryPart: "+ imaginaryValue;
    }

	public void check() {
		System.out.println("I'm in ComplexNumber Class");
		super.ping();
		System.out.println("Checking ended.");	
	}
}
