package lab01_part02;

//task08

public class Date {
	private int month;
	private int day;
	private int year;
	public Date() {
		month = 0;
		day = 0;
		year = 0;
	}
	public void setMonth(int m) {
		month = m;
	}
	public void setDay(int d) {
		day = d;
	}
	public void setYear(int y) {
		year = y;
	}
	
	public int getMonth() {
		return month;
	}
	public int getDay() {
		return day;
	}
	public int getYear() {
		return year;
	}
	
	public String displayDate() {
		return month + " / " + day + " / " + year; 
	}
	
	public static void main(String[] args) {
		Date d = new Date();
		d.setMonth(2);
		d.setDay(12);
		d.setYear(2018);
		System.out.println(d.displayDate());
	}
}
