package lab01_part02;

import java.util.Scanner;
public class task03 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x, n = sc.nextInt();
		int array[] = new int[n];
		try {
			array[5] = 99;
		} catch(ArrayIndexOutOfBoundsException ai) {
			System.out.println(ai);
		}
		try{
			x = n / 0;
		} catch(ArithmeticException ae) {
			System.out.println(ae);
		} finally {
			System.out.println("The End!");
		}
	}
}
