package lab01_part02;

import java.util.Scanner;
public class task05 {
	
	public static int readInteger(String s) throws EitaIntegerNoiException {
//		double d = Double.valueOf(s);
//		if (d == (int) d) {
//			return (int) d;
//		}
//		else {
//			throw new EitaIntegerNoiException();
//		}
		
		if (!s.contains(".")) {
			Integer num = Integer.parseInt(s);
			return num;
		}
		else {
			throw new EitaIntegerNoiException();
		}
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		try {
			System.out.println(readInteger(s));
		} catch(EitaIntegerNoiException e) {
			System.out.println(e);
		}
	}
}
