package lab01_part02;

import java.util.*;
public class QuadEqn {
	private int a, b, c;
	
	public QuadEqn() {
		a  = 0; b = 0; c  = 0;
	}
	public QuadEqn(int x, int y, int z) {
		a = x; b = y; c = z;
	}
	public String Equation() {
		String x_part, y_part, z_part;
		
		if (a == 0) x_part = "";
		else if (a == 1) x_part = "x^2";
		else x_part = a + "x^2";
		
		if (b == 0) {
			if (a != 0 && c != 0) y_part = " + ";
			else y_part = "";
		}
		else if (b == 1) {
			if (a != 0 && c != 0) y_part = " + y + ";
			else if (a == 0 && c != 0) y_part = "y + ";
			else y_part = " + y";
		}
		else {
			if (a != 0 && c != 0) y_part = " + " + b + "y + ";
			else if (a == 0 && c != 0) y_part = b + "y + ";
			else y_part = " + " + b + "y";
		}
		
		if (c == 0) z_part = "";
		else {
			if (a == 0 && b == 0) z_part = "" + c;
			else z_part = c + "";
		}
		
		return "Quadratic Equation: " + x_part + y_part + z_part;
	}
	public String First_Root() {
		double First_Root, Second_Root;
		int discriminant = b * b - 4 * a * c;
		if (discriminant > 0) {
			First_Root = (-b + Math.sqrt(discriminant) / (2 * a));
			Second_Root = (-b - Math.sqrt(discriminant) / (2 * a));
		} else if (discriminant == 0) {
			First_Root = (-b + Math.sqrt(discriminant) / (2 * a));
		} else {
			return "Sorry! Roots are imaginary.";
		}
		return "First Root is: " + First_Root;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		QuadEqn eqn = new QuadEqn(sc.nextInt(), sc.nextInt(), sc.nextInt());
		System.out.println(eqn.Equation());
		System.out.println(eqn.First_Root());
	}
}
