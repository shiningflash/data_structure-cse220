package lab09_recursion;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 27, 2018
 **/

public class TesterClass {

	public static class LinkedList {

		public static class Node {
			public int element;
			public Node next;

			public Node(int e, Node n) {
				element =e ;
				next = n;
			}
		}

		public Node head;

		public LinkedList(int [] a){
			head=new Node(a[0],null);
			Node lej=head;
			for(int i=1;i<a.length;i++){
				Node notun=new Node(a[i],null);
				lej.next=notun;
				lej=notun; 
			}

		}

		public LinkedList(Node h){
			head=h;
		}

		public void printList(){
			for(Node i=head;i!=null;i=i.next){
				System.out.print(i.element+" ");
			}
			System.out.println("");
		}

		public int countNode(){
			int count=0;
			for(Node i=head;i!=null;i=i.next){
				count++;
			}
			return count;
		}

		public Node nodeAt(int idx){
			int count=0;
			int f=0;
			Node y=null;
			for(Node i=head;i!=null;i=i.next){
				count++;
			}
			if(idx<0 || idx>=count ){
				return null;
			}
			else{
				for(Node i=head;f<=idx;i=i.next){
					y=i;
					f++;
				}
			}
			return y;
		}

		public Node copyList(){
			Node matha =head;
			Node tail=matha;
			for(Node i=head.next;i!=null;i=i.next){
				tail.next=i;
				tail=tail.next;
			}
			return matha;
		}

		public Node reverseList(){
			int count=0,f=0,w=0;
			Node y=null,z=null;
			for(Node i=head;i!=null;i=i.next){
				count++;
			}
			Node matha=null;


			for(Node j=head;f<count;j=j.next){
				z=j;
				f++;
			}
			matha=z;

			for(int i=1;i<count;i++){
				w=0;
				for(Node j=head;w<count-i;j=j.next){
					y=j;
					w++;
				}
				y.next=null;
				z.next=y;
				z=z.next;
			}
			return matha;
		}   

		public void rotateLeft(){
			int count=0,f=0;
			Node z=null;
			for(Node i=head;i!=null;i=i.next){
				count++;
			}
			for(Node j=head;f<count;j=j.next){
				z=j;
				f++;
			}
			Node x=head.next;
			head.next=null;
			z.next=head;
			head=x;

		}

		public void rotateRight(){
			int count=0,f=0;
			Node z=null;
			for(Node i=head;i!=null;i=i.next){
				count++;
			}
			for(Node j=head;f<count-1;j=j.next){
				z=j;
				f++;
			}
			Node x=z.next;
			z.next=null;
			x.next=head;
			head=x;

		}
	}

	public static class DoublyList{
		public dNode head;

		public static class dNode{
			public int element;
			public dNode next;
			public dNode prev;

			public dNode(int e, dNode n, dNode p){
				element =e ;
				next = n;
				prev=p;
			}
		}

		public DoublyList(int [] a){
			head = new dNode(a[0], null, null);
			dNode tail = head;

			for(int i = 1; i<a.length; i++){
				dNode mn = new dNode(a[i], null, null);
				tail.next = mn;
				mn.prev=tail;
				tail=tail.next;
			}
		}
	
		public DoublyList(dNode h){
			head = h;
		}

		public int countNode(){
			int count=0;
			for(dNode i=head.next;i!=head;i=i.next){
				count++;
			}
			return count;
		}

		public void forwardprint(){
			for(dNode i=head;i!=null;i=i.next){
				System.out.print(i.element+" ");
			} 
			System.out.println();
		}
	}

	public static int power(int m, int n) {
		if (n != 0) return m * power(m, n-1);
		return 1;
	}

	public static String decimalToBinary(int decimal) {
		if (decimal > 0) return decimalToBinary(decimal / 2) + "" + (decimal % 2);
		else return "";
	}

	public static void printArray(int data[], int index) {
		if (index != -1) {
			printArray(data, index-1);
			System.out.print(data[index] + " ");
		}
		if (index == data.length-1) System.out.println();
	}

	public static int recursiveFactorial(int n) {
		if (n > 1) return (n * recursiveFactorial(n-1));
		else return 1;
	}

	public static int recursiveFibonacci(int n) {
		if (n < 2) return n;
		else return recursiveFibonacci(n-1) + recursiveFibonacci(n-2);
	}

	public static int minIndex(int a[], int i, int j) {
		if (i == j)  return i;
		int k = minIndex(a, i + 1, j);
		return (a[i] < a[k])? i : k;
	}

	public static void recursiveSelectionSort(int a[], int n, int index) {
		if (index == n) return;
		int k = minIndex(a, index, n-1);  //@param k --> minimum index
		if (k != index) {
			int temp = a[k];
			a[k] = a[index];
			a[index] = temp;
		}
		recursiveSelectionSort(a, n, index + 1);
	}

	public static void recursiveInsertionSort(int arr[], int n) {
		if (n <= 1)  return;
		recursiveInsertionSort(arr, n-1 );
		int last = arr[n-1];
		int j = n-2;
		while (j >= 0 && arr[j] > last) {
			arr[j+1] = arr[j];
			j--;
		}
		arr[j+1] = last;
	}

	public static int linkedListSum(lab09_recursion.TesterClass.LinkedList.Node head){
		if(head==null){
			return 0;
		}
		else{
			return head.element+linkedListSum(head.next);
		}
	}

	public static void printLinkedList(lab09_recursion.TesterClass.LinkedList.Node head){
		if(head==null){}
		else{
			System.out.print(head.element+" ");
			printLinkedList(head.next);
		}
	}

	public static void printDLinkedList(lab09_recursion.TesterClass.DoublyList.dNode head){
		if(head==null){}
		else{
			System.out.print(head.element+" ");
			printDLinkedList(head.next);
		}
	}


	public static void sortArraySelection(int a[],int i){
		if(i<a.length-1){
			int k=selectionMin(a,i,i+1);
			int p=a[k];
			a[k]=a[i];
			a[i]=p;
			sortArraySelection(a,i+1);
		}
	}

	public static int selectionMin(int a[],int min,int i){
		if(i<a.length){
			if(a[i]<a[min]){
				min=i;
			}
			min=selectionMin(a,min,i+1);
		}
		return min;
	}

	public static void sortArrayInsertion(int a[],int i){
		if(i<a.length){
			insertionSwaping(a,i);
			sortArrayInsertion(a,++i);
		}
	}

	public static void insertionSwaping(int a[],int i){
		if(i>0){
			if(a[i]<a[i-1]){
				int k=a[i];
				a[i]=a[i-1];
				a[i-1]=k;
				insertionSwaping(a,i-1);
			}
		}
	}

	public static void sortListSelection(lab09_recursion.TesterClass.LinkedList.Node head){
		if(head!=null){
			lab09_recursion.TesterClass.LinkedList.Node min=selectionListMin(head.next,head);
			int p=min.element;
			min.element=head.element;
			head.element=p;
			sortListSelection(head.next);
		}
	}

	public static lab09_recursion.TesterClass.LinkedList.Node selectionListMin(lab09_recursion.TesterClass.LinkedList.Node next,lab09_recursion.TesterClass.LinkedList.Node head){
		if(next!=null){
			if(next.element<head.element){
				head=next;
			}
			head=selectionListMin(next.next,head);
		}
		return head;
	}

	public static void sortLinkedInsertion(lab09_recursion.TesterClass.DoublyList.dNode head){
		if(head!=null){
			insertionLinkedSwaping(head);
			sortLinkedInsertion(head.next);
		}
	}

	public static void insertionLinkedSwaping(lab09_recursion.TesterClass.DoublyList.dNode head){
		if(head!=null && head.prev!=null){
			if(head.element<(head.prev).element){
				int k=head.element;
				head.element=(head.prev).element;
				(head.prev).element=k;
				insertionLinkedSwaping(head.prev);
			}
		}
	}

	public static void main(String[] args) {
		int a[] = {2, 6, 12, 3312, 1, 0, 3346};
		System.out.println("Factorial of 12: " + recursiveFactorial(12));
		System.out.println("Fibonacci of 5: " + recursiveFibonacci(5));
		System.out.println("Decimal 17 to Binary: " + decimalToBinary(17));
		System.out.print("Printing Array: ");
		printArray(a, a.length-1);
		System.out.println("12 to the power 4: " + power(12, 4));
		
		System.out.print("After Selection sort: ");
		recursiveSelectionSort(a, a.length, 0);
		printArray(a, a.length-1);
		System.out.print("After Insertion sort: ");
		recursiveInsertionSort(a, a.length);
		printArray(a, a.length-1);

		LinkedList p=new LinkedList(a);
		System.out.println("Sum of list: " + linkedListSum(p.head));
		System.out.print("Print linked list: ");
		printLinkedList(p.head);
		System.out.println();
		
		System.out.print("After selection sort (list): ");
		sortListSelection(p.head);
		printLinkedList(p.head);
		System.out.println();

		DoublyList n=new DoublyList(a);
		System.out.print("Print doubly linked list: ");
		printDLinkedList(n.head);
		System.out.println();
		System.out.print("After Insertion sort (doubly list): ");
		sortLinkedInsertion(n.head);
		printDLinkedList(n.head);
		System.out.println();
	}
}
