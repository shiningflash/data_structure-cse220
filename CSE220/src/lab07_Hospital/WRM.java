package lab07_Hospital;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 13, 2018
 **/

public class WRM {
	
    //instance variable
	
    public int selection;
    public static int count = 1;
    public int start = 0;
    public int size = 0;
    Patient front;
    Patient head;
    Patient rear;
    Patient data[] = new Patient[5];
    
    // contructor
    // get selection
    public WRM(int i) {
    	selection = i;
    }

    // Method started

    public void RegisterPatient(Patient e) throws PatientArJaigaNaiException {
        if (size == data.length) throw new PatientArJaigaNaiException();
        else if (selection == 1) {
            e.id = count + 100000;
            data[(start+size) % data.length] = e;
            count++;
            size++;
        }
        else if (selection == 2) {
            if (size == 0 ) {
                e.id = count + 100000;
                front = e;
                rear = e;
                size++;
                count++;
            }
            else {
                e.id = count + 100000;
                rear.next = e;
                rear = e; // rear = rear.next
                size++;
                count++;
            }
        }
    }

    public void servePatient() throws PatientNaiException {
    	if (size == 0) throw new PatientNaiException();
    	else if (selection == 1) {
    		data[start % data.length] = null;
    		start++;
    		size--;
    	}
    	else if (selection == 2) {
    		front = front.next;
    		size--;
    	}
    }
    
    public void cancelAll() {
    	if (selection == 1) {
    		if (size != 0) {
    			for (int i = 0; i < size; i++) {
    				data[i] = null;
    			}
    		}
    		size = 0;
    	}
    	else if (selection == 2) {
    		front = null;
    		rear = null;
    		size = 0;
    	}
    }
    
    public boolean canDoctorGoHome() {
    	return (size == 0);
    }
    
    public void showAllPatient() {
    	if (selection == 1) {
    		if (size != 0) {
    			Patient[] serial = new Patient[size];
    			for (int i = 0; i < size; i++) {
    				serial[i] = data[(i+start) % data.length];
    			}
    			
    			for (int i = 0; i < serial.length; i++) {
    				Patient max = serial[i];
    				int maxlocation = i;
    				for (int j = i+1; j < serial.length; j++) {
    					if ((serial[j].name).compareTo(max.name) < 0) {
    						max = serial[j];
    						maxlocation = j;
    					}
    				}
    				serial[maxlocation] = serial[i];
    				serial[i] = max;
    			}
    			for (int i = 0; i < size; i++) {
    				System.out.println(serial[i].name + "  Id : " + serial[i].id);
    			}
    			System.out.println();
    		}
    		else if (size == 0) {
    			System.out.println("There is no patient");
    		}
    	}
    	
    	else if (selection == 2) {
    		if (size != 0) {
    			head = new Patient(front.name, front.age, front.bloodgroup);
    			head.id = front.id;
    			Patient last = head;
    			
    			for (Patient i = front.next; i != null; i = i.next) {
    				Patient p = new Patient(i.name, i.age, i.bloodgroup);
    				p.id = i.id;
    				last.next = p;
    				last = p;
    			}
    			
    			for (Patient i = head; i != null; i = i.next) {
    				Patient max = i;
    				for (Patient j = i.next; j != null; j = j.next) {
    					if ((j.name).compareTo(max.name) < 0) {
    						max = j;
    					}
    				}
    				System.out.println(max.name + "  Id : " + max.id);
    				String k = max.name;
    				max.name = i.name;
    				i.name = k;
    			}
    		}
    		else if (size == 0) {
    			System.out.println("There is no patient");
    		}
    	}
    }
    
}