package lab07_Hospital;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 13, 2018
 **/

import java.util.Scanner;
public class Tester {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		char press;
		int enter;
		do {
			System.out.println("Enter 1 to select array based queue");
			System.out.println("Enter 2 to select linked list based queue");
			enter = sc.nextInt();
			while (enter != 1 && enter != 2) {
				System.out.println("Please Enter 1 or 2 only");
				enter = sc.nextInt();
			}
			try
			{
				WRM w = new WRM(enter);
				w.RegisterPatient(new Patient("Bagdad Ahmed", 21, "A+"));
					w.RegisterPatient(new Patient("Samiul Islam", 21, "O+"));
						w.RegisterPatient(new Patient("Leonel Messi", 21, "A+"));
							w.RegisterPatient(new Patient("Irina Hoque", 21, "AB+"));
				w.showAllPatient();
				w.servePatient();
				w.servePatient();
				System.out.println("Doctor " + ((w.canDoctorGoHome()) ? "can" : "can\'t") + " go home\n");
				
				w.RegisterPatient(new Patient("Neymar Jr.", 21, "O-"));
					w.RegisterPatient(new Patient("Herry Kane", 21, "AB-"));
						w.RegisterPatient(new Patient("Luka Modric", 21, "A+"));
				w.showAllPatient();
				w.cancelAll();
				w.showAllPatient();
				System.out.println("Doctor " + ((w.canDoctorGoHome()) ? "can" : "can\'t") + " go home\n");
			} catch (Exception e) {
				System.out.println(e);
			}
			
			System.out.println("\n");
			System.out.println("If you want to continue, press \'Y\'");
			System.out.println("Press any key, otherwise");
			press = sc.next().charAt(0);
			System.out.println("\n");
		} while (press == 'y' || press == 'Y');
		
	}

}
