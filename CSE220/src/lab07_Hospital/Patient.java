package lab07_Hospital;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Jul 13, 2018
 **/

public class Patient{
	String name = "";
	String bloodgroup = "";
	int age = 0;
	int id = 0;
	Patient next;
	
	public Patient(String n, int a, String bg){
		name = n;
		age = a;
		bloodgroup = bg;
	}
}