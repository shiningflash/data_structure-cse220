package lab02;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    May 6, 2018
 **/

import java.util.Arrays;

public class LinearArray {

	public static void main(String[] args) {

		int [] a = {10, 20, 30, 40, 50, 60};
	    
	    System.out.println("\n///// TEST 01: Copy Array example /////");
	    int [] b = copyArray(a, a.length);
	    printArray(b); // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    
	    System.out.println("\n///// TEST 02: Print Reverse example /////");
	    printArray(a); // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    printReverse(a); // This Should Print: { 60, 50, 40, 30, 20, 10 } 
	    
	    
	    System.out.println("\n///// TEST 03: Reverse Array example /////");
	    reverseArray(b);
	    printArray(b); // This Should Print: { 60, 50, 40, 30, 20, 10 } 
	    
	    
	    System.out.println("\n///// TEST 04: Shift Left k cell example /////");
	    b = copyArray(a, a.length);
	    b=shiftLeft(b,3);
	    printArray(b); // This Should Print: { 40, 50, 60, 0, 0, 0 } 
	    
	    System.out.println("\n///// TEST 05: Rotate Left k cell example /////");
	    b = copyArray(a, a.length); 
	    printArray(b); // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    b=rotateLeft(b,3);
	    printArray(b); // This Should Print: { 40, 50, 60, 10, 20, 30 } 
	    
	    System.out.println("\n///// TEST 06: Shift Right k cell example /////");
	    b = copyArray(a, a.length);
	    printArray(b); // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    b=shiftRight(b,3);
	    printArray(b);  // This Should Print: { 0, 0, 0, 10, 20, 30 } 
	    
	    System.out.println("\n///// TEST 07: Rotate Right k cell example /////");
	    b = copyArray(a, a.length);
	    printArray(b); // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    b=rotateRight(b,3);
	    printArray(b); // This Should Print: { 40, 50, 60, 10, 20, 30 } 
	    
	    
	    System.out.println("\n///// TEST 08: Insert example 1 /////");
	    
	    b = copyArray(a, a.length);
	    printArray(b);  // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    boolean bol=insert(b,6, 70, 2); // This Should Print: No space Left 
	    System.out.println(bol); // This Should Print: false
	    printArray(b);  // This Should Print: { 10, 20, 30, 40, 50, 60 } 
	    
	    System.out.println("\n///// TEST 09: Insert example 2 /////");
	    int [] c = {10, 20, 30, 40, 50, 0, 0}; 
	    printArray(c);  // This Should Print: { 10, 20, 30, 40, 50, 0, 0 }
	    bol=insert(c,5, 70, 2);  // This Should Print: Number of elements after insertion: 6
	    System.out.println(bol); // This Should Print: true
	    printArray(c); // This Should Print: { 10, 20, 70, 30, 40, 50, 0 } 
	    
	    System.out.println("\n///// TEST 10: Insert example 3 /////");
	    printArray(c); // This Should Print: { 10, 20, 70, 30, 40, 50, 0 } 
	    bol=insert(c,6, 70, 6); // This Should Print: Number of elements after insertion: 7 
	    System.out.println(bol); // This Should Print: true
	    printArray(c); // This Should Print: { 10, 20, 70, 30, 40, 50, 70 } 
	    
	    System.out.println("\n///// TEST 11: Remove example 1 /////");
	    
	    printArray(c); // This Should Print: { 10, 20, 70, 30, 40, 50, 70 } 
	    bol=removeAll(c,7,90);
	    System.out.println(bol); // This Should Print: false
	    printArray(c); // This Should Print: { 10, 20, 70, 30, 40, 50, 70 } 
	    
	    System.out.println("\n///// TEST 12: Remove example 2 /////");
	    printArray(c);  // This Should Print: { 10, 20, 70, 30, 40, 50, 70 } 
	    bol=removeAll(c,7,70);
	    System.out.println(bol); // This Should Print: true
	    printArray(c);  // This Should Print: { 10, 20, 30, 40, 50, 0, 0 }    
	  }
	  
	  // Prints the contents of the source array
	  public static void printArray(int [] source) {
		  int len = source.length;
		    for (int i = 0; i < len; i++) {
		    	if (i == 0) System.out.print("{ " + source[i]);
		    	else if (i == len-1) System.out.print(", " + source[i] + " }");
		    	else System.out.print(", " + source[i]);
		    }
		    System.out.println();
		  
		// Another way (toString print)
//	    System.out.println(Arrays.toString(source));
	  }
	  
	  // Prints the contents of the source array in reverse order
	  public static void printReverse(int [] source){
	    int len = source.length;
	    for (int i = len-1; i >= 0; i--) {
	    	if (i == len-1) System.out.print("{ " + source[i]);
	    	else if (i == 0) System.out.print(", " + source[i] + " }");
	    	else System.out.print(", " + source[i]);
	    }
	    System.out.println();
	  }
	  
	  // creates a copy of the source array and returns the reference of the newly created copy array
	  public static int [] copyArray(int [] source, int len){
	    int copyArray[] = java.util.Arrays.copyOf(source, len); // copied source to copyArray
	    return copyArray;    
	  }
	  
	  // creates a reversed copy of the source array and returns the reference of the newly created reversed array
	  public static void reverseArray(int [] source){
		  int len = source.length, temp, i = 0;
		  int j = len-1;
		  while(i < j) {
			  temp = source[i];
			  source[i] = source[j];
			  source[j] = temp;
			  i++; j--;
		  }
		  printArray(source);
	  }
	  
	  // Shifts all the elements of the source array to the left by 'k' positions
	  // Returns the updated array
	  public static int [] shiftLeft(int [] source, int k){
	    int len = source.length;
	    for (int i = k; i < len; i++) {
	    	source[(i-k) % len] = source[i]; // left shifted by 'k' positions
	    	source[i] = 0;
	    }
	    return source;	// returns updated array    
	  }
	  
	  // Rotates all the elements of the source array to the left by 'k' positions
	  // Returns the updated array
	  public static int [] rotateLeft(int [] source, int k){
		int len = source.length;
	    int rotateLeft[] = new int[len];
	    for (int i = 0; i < len; i++) {
	    	rotateLeft[(i+k) % len] = source[i];
	    }
	    return rotateLeft; // return array rotated left 'k' cell   
	  }
	  
	  // Shifts all the elements of the source array to the right by 'k' positions
	  // Returns the updated array
	  public static int [] shiftRight(int [] source, int k){
		  int len = source.length;
		    for (int i = 0; i < k; i++) {
		    	source[(i+k) % len] = source[i]; // right shifted by 'k' positions
		    	source[i] = 0;
		    }
		    return source;	// returns updated array    
	  }
	  
	  // Rotates all the elements of the source array to the right by 'k' positions
	  // Returns the updated array
	  public static int [] rotateRight(int [] source, int k){
		  int len = source.length;
		    int rotateRight[] = new int[len];
		    for (int i = 0; i < len; i++) {
		    	rotateRight[(i-k+len) % len] = source[i];
		    }
		    return rotateRight; // return array rotated right 'k' cell
	  }
	  
	  /** @return true if insertion is successful; @return false otherwise
	    * @param arr the reference to the linear array
	    * @param size the number of elements that exists in the array. size<=arr.length
	    * @param elem the element that is to be inserted
	    * @param index the index in which the element is required to be inserted
	    * if insertion is not successful due to lack space, print "No space Left"
	    * if given index is invalid, print "Invalid Index"
	    * if insertion is successful, print the 'Number of elements after insertion' is completed
	    */
	  public static boolean insert(int [] arr, int size, int elem, int index){
	    int capacity = arr.length;
	    if (size >= capacity) {
	    	System.out.println("No space left");
	    	return false;
	    } else if (index >= capacity) {
	    	System.out.println("Invalid Index");
	    	return false;
	    } else {
	    	int repl = arr[index];
	    	arr[index] = elem;
	    	int temp, i;
	    	for (i = index+1; i <= size; i++) {
	    		temp = arr[i];
	    		arr[i] = repl;
	    		repl = temp;
	    	}
	    	System.out.println("Number of elements after insertion: " +  i);
	    	return true;
	    }
	  }
	  
	  /** 
	   * removes all the occurrences of the given element
	   * @param arr the reference to the linear array
	   * @param size the number of elements that exists in the array. size<=arr.length
	   * @param elem the element to be removed
	   * @return true if removal is successful; return false otherwise
	   * if removal is successful, print the 'Number of elements after removal' is completed
	   */  
	  public static boolean removeAll(int [] arr, int size, int elem){
		  boolean flag = false;
		  int k = 0;
		  for (int i = 0; i < size; i++) {
			  if (arr[i] == elem) { 
				  ++i;
				  if (i < size) {
					  arr[k++] = arr[i];
					  flag = true;
				  }
			  } else arr[k++] = arr[i];
		  }
		  if (flag)
			  for (int i = k; i < size; i++) 
				  arr[i] = 0;
		  return flag;
	  }	
}