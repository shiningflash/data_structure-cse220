package Queue;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

public class person {
	protected String name;
	protected int roll;
	
	public person(String name, int roll) {
		this.name = name;
		this.roll = roll;
	}
	
	public String toString() {
		return "name: " + name + "\troll: " + roll;
	}
}
