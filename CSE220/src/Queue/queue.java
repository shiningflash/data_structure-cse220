package Queue;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/


import java.util.*;
public class queue {
	
	private int[] q;
	private int size;
	private int total;
	private int front;
	private int rear;
	
	public queue() {
		size = 5;
		total = 0;
		front = 0;
		rear = 0;
		q = new int[size];
	}
	public queue(int size) {
		this.size = size;
		total = 0;
		front = 0;
		rear = 0;
		q = new int[this.size];
	}
	
	public boolean enqueue(int item) {
		if (isFull()) return false;
		else {
			total++;
			q[rear++] = item;
			return true;
		}
	}
	public int dequeue() {
		int item = q[front];
		total--;
		front = (front + 1) % size;
		return item;
	}
	
	public boolean isFull() {
		return (total == size);
	}
	
	public void showAll() {
		int f = front;
		if (size != 0) {
			for (int i = 0; i < size; i++) {
				System.out.print(q[f] + " ");
				f = (f + 1) % size;
			}
		}
	}
}
