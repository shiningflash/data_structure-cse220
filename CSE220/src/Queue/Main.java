package Queue;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

public class Main {
	public static void main(String[] args) {
		
		queue q = new queue();
		
		q.enqueue(123);
		q.enqueue(678);
		q.enqueue(456);
		
		System.out.println(q.dequeue());
		System.out.println(q.dequeue());
		System.out.println(q.dequeue());
		System.out.println(q.dequeue());
		System.out.println(q.dequeue());
		
		System.out.println();
		
		q.showAll();

		System.out.println();
		System.out.println();

//		System.out.println(q.dequeue());
		
		person p1 = new person("Amirul", 1710);
		person p2 = new person("Samiul", 1748);
		
		System.out.println(p1);
		System.out.println(p2);
		
		personQueue p = new personQueue();
		
		p.enqueue(new person("Bahadur", 1720));
		p.enqueue(new person("Fahim", 1732));
		
		p.showAll();
		
		
		/*
		 * Output:
		 * 123
		 * 678
		 * 456
		 * 0
		 * 0
		 * 
		 * 123 678 456 0 0 
		 * 
		 * name: Amirul	roll: 1710
		 * name: Samiul	roll: 1748
		 * 
		 * name: Bahadur	roll: 1720 
		 * name: Fahim	roll: 1732 
		 */
	}
}
