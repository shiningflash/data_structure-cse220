package Queue;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    April 20, 2018
 **/

import java.util.*;
public class personQueue {
	protected person que[];
	protected int size;
	protected int total;
	protected int front;
	protected int rear;
	
	public personQueue() {
		size = 5;
		total = 0;
		front = 0;
		rear = 0;
		que = new person[size];
	}
	
	public personQueue(int size) {
		this.size = size;
		total = 0;
		front = 0;
		rear = 0;
		que = new person[this.size];
	}
	
	public boolean enqueue(person item) {
		if (isFull()) return false;
		else {
			total++;
			que[rear] = item;
			rear++;
			return true;
		}
	}
	
	public person dequeue() {
		person item = que[front];
		total--;
		front++;
		return item;
	}
	
	public boolean isFull() {
		if (total == size)
			return true;
		else
			return false;
	}
	public void showAll() {
		int f = front;
		if (total != 0) {
			for (int i = 0; i < total; i++) {
				System.out.print("\n" + que[f].toString() + " ");
				f = (f + 1) % size;
			}
		}
	}
}
